import com.training.tddone.Calculator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void testAddWithTwoInputs() {
        Calculator calculator = new Calculator();
        int result = calculator.add("5+3");
        assertEquals(8, result);

        int result2 = calculator.add("5+0");
        assertEquals(5, result2);
    }

    @Test
    public void testAddWithEmptyString() {
        Calculator calculator = new Calculator();
        int result = calculator.add("");
        assertEquals(0, result);
    }

    @Test
    public void testAddWithSingleInput() {
        Calculator calculator = new Calculator();
        int result = calculator.add("2");
        assertEquals(2, result);
    }

    @Test
    public void testAddWithNegativeValues() {
        Calculator calculator = new Calculator();
        int result = calculator.add("-2+5");
        assertEquals(3, result);

        int result2 = calculator.add("3+-5");
        assertEquals(-2, result2);


        int result3 = calculator.add("-3+-5");
        assertEquals(-8, result3);
    }


    @Test
    public void testAddWithNonNumber() {
        Calculator calculator = new Calculator();
        int result = calculator.add("gfd");
        assertEquals(0, result);

        int result2 = calculator.add("gfd+rew");
        assertEquals(0, result2);


        int result3 = calculator.add("5 + 6");//Unwanted space
        assertEquals(0, result3);
    }




}
