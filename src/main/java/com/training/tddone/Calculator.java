package com.training.tddone;

public class Calculator {

    public int add(String input) {
        input = input.trim();
        if (input.isEmpty()) {
            return 0;
        }
        String[] inputs =  input.split("\\+");
        int output = 0;
        for (String operand : inputs) {
            if (isNumberWithOrWithoutNegative(operand) == false) {
                output = 0;
                break;
            }
            int operandNum = Integer.parseInt(operand);
            output = output + operandNum;
        }
        return output;
    }

    private boolean isNumberWithOrWithoutNegative(String input) {
        String inputWithoutNegative = input.replaceFirst("^-", "");
        return inputWithoutNegative.matches("[0-9]+");
    }
}
